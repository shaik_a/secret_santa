class AddSantaIdToRegistrations < ActiveRecord::Migration[5.0]
  def change
    add_column :registrations, :santa_id, :integer
  end
end
