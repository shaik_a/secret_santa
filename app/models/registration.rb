class Registration < ApplicationRecord

  validates :employee_id, presence: true, uniqueness: true
  validates :name, presence: true
  validates :email, presence: true, uniqueness: true
  validates_inclusion_of :employee_id, :in => 1..3000
  belongs_to :santa, class_name: "Registration", optional: true

  def self.assign_santa_id
    registrations = all.sort_by{ rand }
    registrations.each_with_index do |registration, index|
      next_registration = registrations[index+1] || registrations.first
      next_registration.update_attribute(:santa, registration)
    end
  end
  def self.send_sigmaties_to_santa
    registrations = all
    registrations.each do |sigmaite|
      SecretSantaMailer.send_sigmaite_to_santa(sigmaite).deliver_later
    end
  end

  def self.send_clues
    registrations = all
    registrations.each do |sigmaite|
      SecretSantaMailer.send_clue(sigmaite).deliver_later
    end
  end

  def clue_name
    clue = santa.name
    (0..clue.size).step(3){ |n| clue[n] = '_' unless clue[n] == ' ' }
    return clue.to_s
  end

  def clue_id
    clue_id = santa.employee_id.to_s
    clue_id[1] = '_'
    return clue_id
  end

end
