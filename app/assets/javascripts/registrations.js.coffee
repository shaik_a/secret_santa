# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$.ajaxSetup({
  dataType: 'json'
})


window.snowEffects = () -> 
  $(document).snowfall (
	  flakeCount: 100
	  maxSpeed: 3
	  round: true
	  minSize: 3
	  maxSize: 7
	)

$( () ->
  $(".new_registration,.edit_registration").on("ajax:complete", (e, data, status, xhr) ->  
    return unless status is 'success'
    window.location.href = '/registrations/' + data.responseJSON.id
  ).on("ajax:error", (e, data, status, xhr) ->
    errors = data.responseJSON
    $.each errors, (key, element) ->
      $('label#'+key).addClass('is-invalid-label')
      $('label#'+key).find('input').addClass('is-invalid-input')
      $('label#'+key).find('small.form-error').addClass('is-visible').text(element)
  )
)