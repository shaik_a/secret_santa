class SecretSantaMailer < ApplicationMailer
default from: "secretsanta@sigmainfo.net"

  def registered_santa(employee)
    email_images
    @employee = employee
    mail(to: @employee.email, subject: 'Secret Santa: Registration successful')
  end

  def send_sigmaite_to_santa(sigmaite)
    email_images
    @sigmaite = sigmaite
    mail(to: @sigmaite.santa.email, subject: 'Secret Santa: Your Sigmaite')
  end

  def send_clue(sigmaite)
    email_images
    @sigmaite = sigmaite
    mail(to: @sigmaite.email, subject: 'Secret Santa: Can you Guess Who ?')
  end

  private

  def email_images
    attachments.inline['logo.png'] = File.read('app/assets/images/secret_santa.gif')
  end
end
