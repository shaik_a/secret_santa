class RegistrationsController < ApplicationController
  before_action :set_registration, only: [:show, :edit, :update, :destroy]
  before_action :admin_access, only:[:index]
  before_action :registrations_closed , only:[:new, :edit]
  # GET /registrations
  # GET /registrations.json
  def index
    @registrations = Registration.all
  end

  def landing
    
  end

  def rules

  end

  # GET /registrations/1
  # GET /registrations/1.json
  def show
  end

  # GET /registrations/new
  def new
    @registration = Registration.new
  end

  # GET /registrations/1/edit
  def edit
  end

  def close_registration
    registration_lock = AdminConfig.where(name:'registration_lock').first.update_attribute(:value,'true')
    redirect_to :root
  end

  # POST /registrations
  # POST /registrations.json
  def create
    @registration = Registration.new(registration_params)
    respond_to do |format|
      if @registration.save 
        SecretSantaMailer.registered_santa(@registration).deliver_later
        format.json { render json: @registration, status: :ok}
      else
        format.json { render json: @registration.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /registrations/1
  # PATCH/PUT /registrations/1.json
  def update
    respond_to do |format|
      if @registration.update(registration_params) 
        SecretSantaMailer.registered_santa(@registration).deliver_later
        format.json { render json: @registration, status: :ok}
      else
        format.json { render json: @registration.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /registrations/1
  # DELETE /registrations/1.json
  def destroy
    @registration.destroy
    respond_to do |format|
      format.html { redirect_to registrations_url }
      format.json { head :no_content }
    end
  end

  def assign_santa_id
    Registration.assign_santa_id unless Rails.application.config.already_assignments_done
    assignment_lock = AdminConfig.where(name:'assignment_lock').first.update_attribute(:value,'true')
    redirect_to :root
  end

  def send_sigmaites
    Registration.send_sigmaties_to_santa
    redirect_to :root
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_registration
      @registration = Registration.find(params[:id])
    end

    def admin_access
      @admin = params[:admin]
      redirect_to landing_registrations_url unless @admin
    end

    def registrations_closed
      redirect_to landing_registrations_url if Rails.application.config.registrations_closed 
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def registration_params
      params.require(:registration).permit(:employee_id, :name, :email)
    end
end
