module RegistrationsHelper
  def assignments_done?
    assignment_lock = AdminConfig.find_by_name('assignment_lock').value
    assignment_lock == 'true'
  end

  def registrations_closed?
    assignment_lock = AdminConfig.find_by_name('registration_lock').value
    assignment_lock == 'true'
  end
end
