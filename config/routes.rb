Rails.application.routes.draw do
  resources :registrations do
  	collection do
  			get :landing
        get :rules
        get :assign_santa_id
        get :close_registration
        get :send_sigmaites
  		end
  	end

  root to: 'registrations#landing'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
