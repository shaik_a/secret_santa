require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module SecretSanta
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.assets.paths << Rails.root.join("app", "assets", "fonts")

    config.registrations_closed     = false
    config.already_assignments_done = false

    config.action_mailer.delivery_method = :smtp
    # SMTP settings for gmail
    config.action_mailer.smtp_settings = {
      :address              => "smtp.gmail.com",
      :port                 => 587,
      :domain               => 'sigmainfo.net',
      :user_name            => 'secretsanta@sigmainfo.net',
      :password             => 'secretsanta@321',
      :authentication       => :login,
      :enable_starttls_auto => true,
      :openssl_verify_mode  => 'none'
    }
  end
end
