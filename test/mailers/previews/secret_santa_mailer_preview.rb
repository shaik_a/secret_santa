# Preview all emails at http://localhost:3000/rails/mailers/secret_santa_mailer
class SecretSantaMailerPreview < ActionMailer::Preview
  def registered_santa_preview
    SecretSantaMailer.registered_santa(Registration.first)
  end

  def send_sigmaite_to_santa_preview
    SecretSantaMailer.send_sigmaite_to_santa(Registration.first)
  end

  def send_clue_preview
    SecretSantaMailer.send_clue(Registration.first)
  end
end
